<!DOCTYPE html>
<html lang="en">

<head>
	<?php include 'header.php'; ?>
</head>

<body>
    
    <?php include 'menu.php'; ?>
	<?php include 'connect.php';?>
    <div id="logo">
    	<p>Browse API</p>
    </div>
    <div id="wrapper">
		<?php
			include('TMDb.php');

			// Default English language
			$tmdb = new TMDb('55ef3debe8f370b52a84fc4af1c1534c');
			$token = $tmdb->getAuthToken();
			$session = $tmdb->getAuthSession();
			 //Retrieve config when the class is already initialised from TMDb (always new request)
			$config = $tmdb->getConfiguration();
			
			$query="SELECT * FROM items WHERE media_type=1";
			$results=query($query);
			echo "<h3>Movie Posters of your films</h3>";
			while ($row=  mysqli_fetch_row($results)){
				$title=$row[2];
				//echo "$title";
				$result=$tmdb->searchMovie($title);
				//print_r(array_keys($result[results][0]));
				$poster=$result[results][0]['poster_path'];//where the poster is stored in the array
				//echo "$poster";
				$filepath = $poster;
				$image_url = $tmdb->getImageUrl($filepath, TMDb::IMAGE_BACKDROP, 'original');
				
				echo "<img src='$image_url' height=200 width=100>";
			}
		?>
		<br />
		<h3>Will this film be the next in your collection?</h3>
			<?php 
				$upcoming = simplexml_load_file("http://api.traileraddict.com/?featured=yes&count=1"); 

				foreach($upcoming->trailer as $x => $updates) 
				{ 
				   echo $updates->title; 
				   echo '<br>'; 
				   echo '<span style="font-size:x-small">Source: <a href="'. $updates->link .'">TrailerAddict</a></span>'; 
				   echo '<br>'; 
				   //now echo the embedded trailer 
				   echo $updates->embed; 
				   echo '<br><br>';     
					 
					} 
			?> 
    </div>
    <?php include 'footer.php'; ?>

</body>
</html>