<?php
function upload()
{
$max_filesize = ini_get('upload_max_filesize');
//echo "the max filesize is $max_filesize".'<br />';
//echo filesize($_FILES['file']['tmp_name']);
if(filesize($_FILES['file']['name']) > $max_filesize)
{
    die("Your file is greater than $max_filesize <br />");
}

$filename = $_FILES['file']['name'];
$ext = substr($filename, strpos($filename, '.'), strlen($filename)-1);
$ext = strtolower($ext);
$allowed_filetypes = array('.jpg', '.gif', '.pdf', '.jpeg','.png');
if(!in_array($ext, $allowed_filetypes))
{
    die("Not allowed to upload that file");
}

$tmpname = $_FILES['file']['tmp_name'];
$destination = './images/';
$new_filename = $destination.$filename;

if(!is_dir($destination))
{
  mkdir($destination, 0755);
  chmod($destination, 0755);
}
if(move_uploaded_file($tmpname , $new_filename))
{
    //echo " $new_filename Successful copy <br />";
    //set permissions
    chmod($new_filename, 0644);
    //echo "<a href=$destination>here</a>";
}
else
{
    echo "No success <br />";
}
}
?>
