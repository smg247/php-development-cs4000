<!DOCTYPE html>
<html lang="en">

<head>
	<?php include 'header.php'; ?>
</head>

<body>
    
    <?php include 'menu.php'; ?>
    <?php include 'connect.php';?>
    <?php include 'do_upload.php';?>
    <div id="logo">
    	<p>Inventory</p>
    </div>
    <div id="wrapper">
		<h3>Add to the collection!</h3>
		<?php
		session_start();//lets try to save the item_id in a session
		$_SESSION['id'];
		echo "$itemID";
		$edit='false';
		if ($_GET['action']=='edit')//lets set some values for editing
		{
			$_SESSION['id']= $_GET[id];//take that, the session works!
			echo "$itemID";
			$edit='true';
			$query="SELECT * FROM items WHERE item_id=$_GET[id]";
			$results = query($query);
			while ($row=  mysqli_fetch_row($results)){
				$title=$row[2];
				//echo "$title";
				$des=$row[3];
				$type=$row[4];
				$rating=$row[5];
				$year=$row[8];
			}

		}else{//this is what will happen if there is no editing being done
			
			$title='';
			$des='';
			$type='';
			$rating='';
			$year='';
		}
		?>
       	<form action="inventory.php" method="post" enctype="multipart/form-data">
    		<label for="Name">Name:</label> 
			<?php 
				//echo $title;
				echo "<input type='text' name='myName' id='Name' value='$title' />"; 
			?>
			<label for="Description">Description:</label> 
			<?php
			echo "<input type='text' name='myDes' id='Description' value='$des' />";
			?>
			<label for="type">Media:</label> 
			<select name="type">
			<?php
				if($type==2){//this stuff is all set up so the media drop down will look good.
				echo "<option value='2'>Book</option>";
				echo "<option value='1'>DVD</option>";
				echo "<option value='3'>Game</option>";
				echo "<option value='4'>CD</option>";
				}
				else if($type==3){
				echo "<option value='3'>Game</option>";
				echo "<option value='2'>Book</option>";
				echo "<option value='1'>DVD</option>";
				echo "<option value='4'>CD</option>";
				}
				else if($type==4){
				echo "<option value='4'>CD</option>";
				echo "<option value='3'>Game</option>";
				echo "<option value='2'>Book</option>";
				echo "<option value='1'>DVD</option>";
				}
				else{
				echo "<option value='1'>DVD</option>";
				echo "<option value='3'>Game</option>";
				echo "<option value='2'>Book</option>";
				echo "<option value='4'>CD</option>";
				}
			?>
			</select>
			<label for="type">Rating:</label>
			<select name="rating">
			<?php
				if($rating==2){
				echo "<option value='2'>2</option>";
				echo "<option value='1'>1</option>";
				echo "<option value='3'>3</option>";
				}
				else if($rating==3){
				echo "<option value='3'>3</option>";
				echo "<option value='2'>2</option>";
				echo "<option value='1'>1</option>";
				}else{
				echo "<option value='1'>1</option>";
				echo "<option value='2'>2</option>";
				echo "<option value='3'>3</option>";
				}
			?>
			</select>
			<label for="Year">Year:</label>
			<?php
			echo "<input type='text' name='myYear' id='Year' value='$year' />";
			?>
			<label for="file">File:</label>
			<input type="file" name="file" id="file">
			<?php
			if($edit=='false')
			{
			echo "<input type='submit' name='submit' id='mySubmit' value='Submit' />";//for adding
			}else if($edit=='true'){
			echo "<input type='submit' name='edit' id='myEdit' value='Edit' />";//for editing 
			}
			?>
		</form>
		<h3>Collection</h3>
		<div id="pics">
			<?php
				$query="SELECT * FROM items";//for displaying the pictures by looking up their names in the database
				$results=query($query);
				while ($row=  mysqli_fetch_row($results)){
					echo "<img src='images/$row[9]' width='50' height='50'>";
				}
			?>
		</div>
		<?php
		$collect = "collection.txt";
		
		$query="SELECT * FROM Users";
		$results=query($query);
		while ($row=  mysqli_fetch_row($results)){
			if($_SESSION['user'] == $row[2]){
				$userid = $row[0];
			}
		}
		
		$myName = $_POST['myName'];
        $myDes = $_POST['myDes'];
        $myType = $_POST['type'];
        $myRating = $_POST['rating'];
        $myYear = $_POST['myYear'];
        $filename = $_FILES['file']['name'];
		if (isset($_POST['myName'])) {
			if(isset($_POST['submit']))//need to make some adjustments to this code
			{
			//if there is no editing
			$query = "INSERT into items VALUES(NULL, '$userid' ,'$myName', '$myDes', '$myType', '$myRating', NULL, NULL, '$myYear','$filename')";
			}else if(isset($_POST['edit'])){
			//if there is editing
			$id = $_SESSION['id'];
			$query = "UPDATE items SET title= '$myName', description= '$myDes', media_type= '$myType', rating_id= '$myRating', year= '$myYear', picture='$filename' WHERE item_id='$id'";
			}
    		query($query);
    		if(isset($_FILES['file']['name']))
    		{
    		upload();//uploads the file to the server
    		header("Location: inventory.php");
    		}
        }
	$query="SELECT * FROM user_roles";//for checking to see if the current logged in user is an admin
	$results=query($query);
	while ($row=  mysqli_fetch_row($results)){
		if($row[0]==$userid){
			$role = $row[1];
		}
	}
	if ($_GET['action']=='delete') {//deleting goes on here
    $query = "DELETE FROM items WHERE item_id=$_GET[id]";
    query($query);
    $query = "DELETE FROM picture WHERE pic_id=$_GET[id]";
    query($query);
    header("Location: inventory.php");
	}
	if($role!=1){
        	$query="SELECT * FROM items WHERE user_id='$userid'";//add a WHERE to this so each user can only see the items associated with their ID
        }else{
        	$query="SELECT * FROM items";
        }
		$results=query($query);
		echo "<table border=1>";
		echo "<tr>";
		echo "<th>User</th>";
		echo "<th>Title</th>";
		echo "<th>Description</th>";
		echo "<th>Type</th>";
		echo "<th>Rating</th>";
		echo "<th>Year</th>";
		echo "<th>Del</th>";
		echo "<th>Edit</th>";
		echo "</tr>";
		while ($row=  mysqli_fetch_row($results)){
			echo "<tr>";
			echo "<td>$row[1]</td>";
    		echo "<td>$row[2]</td>";
    		echo "<td>$row[3]</td>";
    		echo "<td>$row[4]</td>";
    		echo "<td>$row[5]</td>";
    		echo "<td>$row[8]</td>";
    		echo "<td><a href=./inventory.php?action=delete&id=$row[0]>del</a></td>";
    		echo "<td><a href=./inventory.php?action=edit&id=$row[0]>edit</a></td>";
    		echo "</tr>";
		}
		echo "</table>";
		/*$fh = fopen($collect, 'r');
		//this is where we will read from the file and print it...
		$theData = fread($fh, filesize($collect));
		fclose($fh);
		echo $theData;
		*/
		?>		
    </div>
    <?php include 'footer.php'; ?>

</body>
</html>