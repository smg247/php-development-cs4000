<!DOCTYPE html>
<html lang="en">

<head>
	<?php include 'header.php'; ?>
</head>

<body>
<?php include 'connect.php'; ?>
<?php include('calendar.class.php'); ?><!--getting the calendar in here to add some functionality-->

	<?php 
	session_start();
		if(!isset($_SESSION['user']))
		{
			header("Location: http://sgoeddel.php.cs.dixie.edu/scheduler/login.php");
		}
		$query="SELECT * FROM Users";//getting the current users id
		$results=query($query);
		while ($row=  mysqli_fetch_row($results)){
			if($_SESSION['user'] == $row[2]){
				$userid = $row[0];
			}
		}
	?>
	<img src="calendar.png">
	<div id="logo">
	
    	<p>Ultimate Scheduler</p>
    </div>
    <?php include 'menu.php'; ?>
	<?php $curDate= date("Y-n-j"); ?>
    <div id="wrapper">
		<div id="homeWrap">
		<h1>Add an Event</h1>
		<form action="index.php" method="post"><!--the form for users to input an event-->
    		<label for="title">Title*:</label> 
			<input type="text" name="title" id="title" />
			<label for="start">Start Date*:</label> 
			<?php echo "<input type='text' name='start' value ='$curDate' id='start' />"; ?>
			<label for="end">End Date*:</label> 
			<?php echo "<input type='text' name='end' value='$curDate' id='end' />"; ?>
			<label for="time">Time:</label> 
			<input type="text" name="time" id="time" />
			<label for="location">Location:</label> 
			<input type="text" name="location" id="location" />
			<label for="details">Details:</label> 
			<textarea rows="2" cols="20" name="details">
			</textarea><br>
			<input type="submit" name="submit" id="mySubmit" value="Submit">
    	</form>  
		</div>
		<div id="today">
	<?php
	   $title = $_POST['title'];//grabbing the form data
       $start = $_POST['start'];
       $end = $_POST['end'];
       $time = $_POST['time'];
       $location = $_POST['location'];
       $details = $_POST['details'];
       $curDate= date("Y-n-j");
		if (isset($_POST['start'])) {
		$query="INSERT INTO events VALUES(NULL,'$userid' ,'$start', '$end', '$time', '$location','$details','$title')";//putting the data into the database
		query($query);
		}
		$query="SELECT * FROM events WHERE user_id='$userid'";//displaying the current users events
		$results=query($query);
		echo"<h2>Today's Events</h2>";
		echo "<table border=1>";
		echo "<tr>";
		echo "<th>Title</th>";
		echo "<th>Start</th>";
		echo "<th>End</th>";
		echo "<th>Time</th>";
		echo "<th>Location</th>";
		echo "<th>Details</th>";
		echo "</tr>";
		while ($row=  mysqli_fetch_row($results)){
			if(strcasecmp($curDate,$row[2])==0)//for displaying only today's events
			{
			echo "<tr>";
			echo "<td>$row[7]</td>";//title
			echo "<td>$row[2]</td>";//start
    		echo "<td>$row[3]</td>";//end
    		echo "<td>$row[4]</td>";//time
    		echo "<td>$row[5]</td>";//location
    		echo "<td>$row[6]</td>";//details
    		echo "</tr>";
    		}
    	}
    	echo "</table>";
	?>
	</div>
	<?php
	$edtitle = $_POST['etitle'];//grabbing the form data from edit
	$edstart = $_POST['estart'];
	$edend = $_POST['eend'];
	$edtime = $_POST['etime'];
	$edlocation = $_POST['elocation'];
	$eddetails = $_POST['edetails']; 
	$id = $_SESSION['id'];
	if(isset($_POST['estart'])){//this is where the updating is done
		$query = "UPDATE events SET title= '$edtitle', start= '$edstart', end= '$edend', time= '$edtime', location= '$edlocation', details='$eddetails' WHERE event_id='$id'";
		query($query);
    }
	?>
	<div id="calendar">
	<?php
		$calDate = date("Y-n");
		$cal = new CALENDAR('full',"$calDate");
		$cal->basecolor = '00CCFF';
		$query="SELECT * FROM events WHERE user_id='$userid'";//displaying the current users events
		$results=query($query);
		while ($row=  mysqli_fetch_row($results)){//adding events to the calendar
			$cal->addEvent(
		array(
			"title"=>"$row[7]", // (required)
			"from"=>"$row[2]", // (required) YYYY-M-D
			"to"=>"$row[3]", // (required) YYYY-M-D
			"starttime"=>"$row[4]", // (optional)
			"endtime"=>"", // (optional)
			"color"=>"#D8E5F9", // (required)
			"location"=>"$row[5]", // (optional)
			"details"=>"$row[6]", // (optional)
			"link"=>"" // (optional)
			)
		);
		}
		echo $cal->showcal();
	?>
	</div>
	<div id="all">
	<?php
	if ($_GET['action']=='delete') {//deleting goes on here
    	$query = "DELETE FROM events WHERE event_id=$_GET[id]";
    	query($query);
		header("Location: index.php");
	}
	
	$query="SELECT * FROM events WHERE user_id='$userid'";//displaying the current users events
		$results=query($query);
		echo"<h2>All Events</h2>";
		echo "<table border=1>";
		echo "<tr>";
		echo "<th>Title</th>";
		echo "<th>Start</th>";
		echo "<th>End</th>";
		echo "<th>Time</th>";
		echo "<th>Location</th>";
		echo "<th>Details</th>";
		echo "<th>Del</th>";
		echo "<th>Edit</th>";
		echo "</tr>";
		while ($row=  mysqli_fetch_row($results)){
			echo "<tr>";
			echo "<td>$row[7]</td>";//title
			echo "<td>$row[2]</td>";//start
    		echo "<td>$row[3]</td>";//end
    		echo "<td>$row[4]</td>";//time
    		echo "<td>$row[5]</td>";//location
    		echo "<td>$row[6]</td>";//details
			echo "<td><a href=./index.php?action=delete&id=$row[0]>del</a></td>";
			echo "<td><a href=./index.php?action=edit&id=$row[0]>edit</a></td>";
    		echo "</tr>";
    	}
    	echo "</table>";
	?>
	<?php
	if ($_GET['action']=='edit')
	{
		$_SESSION['id']= $_GET[id];
		$query="SELECT * FROM events WHERE event_id=$_GET[id]";
		$results = query($query);
		while ($row=  mysqli_fetch_row($results)){
			$etitle=$row[7];
			$estart=$row[2];
			$eend=$row[3];
			$etime=$row[4];
			$elocation=$row[5];
			$edetails=$row[6];
		}
	}
	?>
		<h1>Edit an Event</h1>
		<form action="index.php" method="post"><!--the form for users to edit an event-->
    		<label for="etitle">Title*:</label>
			<?php echo "<input type='text' name='etitle' value='$etitle' id='etitle' />"; ?>
			<label for="estart">Start Date*:</label> 
			<?php echo "<input type='text' name='estart' value='$estart'id='estart' />"; ?>
			<label for="eend">End Date*:</label> 
			<?php echo "<input type='text' name='eend' value='$eend' id='estart' />"; ?><!--need more work with this datepicker...-->
			<label for="etime">Time:</label> 
			<?php echo "<input type='text' name='etime' value='$etime' id='etime' />"; ?>
			<label for="elocation">Location:</label> 
			<?php echo "<input type='text' name='elocation' value='$elocation' id='elocation' />"; ?>
			<label for="edetails">Details:</label> 
			<?php echo"<textarea rows='2' cols='20' name='edetails'>$edetails";
			 echo "</textarea><br>"; ?>
			<input type="submit" name="editsubmit" id="mySubmit" value="Submit">
    	</form> 
    	<?php

       	?>
	</div>
    </div>
    <?php include 'footer.php'; ?>

</body>
</html>