<!DOCTYPE html>
<html lang="en">

<head>
	<?php include 'header.php'; ?>
</head>

<body>

<?php 
	session_start();
	if(isset($_SESSION['user']))
	{
		unset($_SESSION['user']); 
		session_destroy();
	}
	header("Location: http://sgoeddel.php.cs.dixie.edu/scheduler/login.php");
?>
    <?php include 'menu.php'; ?>
    <div id="logo">
    	<p>Logout</p>
    </div>
    <div id="wrapper">
		
    </div>
    <?php include 'footer.php'; ?>

</body>
</html>